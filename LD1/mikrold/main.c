/*
 * mikrold.c
 *
 * Created: 2019-02-13 08:57:04
 * Author : 20163536
 */ 

#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	/*int b[5] = {0b00000001, 0b00000011, 0b00000101, 0b00001001, 0b00010001};*/
	int c[16] = {1, 128, 2, 64, 4, 32, 8, 16, 16, 8, 32, 4, 64, 2, 128, 1};	
	int i = 0;	
	DDRA = 255;
	PORTA = 0b00001111;
    while (1) 
    {
		PORTA = c[i];
		if(i<5)
		{
			i = i + 1;
		}
		
		else
		
		{
			i = 0;
		}
		
	_delay_ms(1000);
    }
}

